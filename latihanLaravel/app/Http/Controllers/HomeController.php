<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use resouces\views;

class HomeController extends Controller
{
    public function utama() {
        return view('welcome');
}
}