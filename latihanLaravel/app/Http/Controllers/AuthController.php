<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.daftar');
    }

    public function home(Request $request)
    {
        $fname = $request->input('fullname');

        return view('page.home', ['fullname' => $fname]);
    }
}
