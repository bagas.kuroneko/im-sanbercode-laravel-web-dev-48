<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [HomeController::class, 'utama'] );

Route::get('/daftar', [AuthController::class, 'daftar'] );
Route::post('/home', [AuthController::class, 'home'] );

Route::get('/master', function (){
    return view ('layouts/master');
});

Route::get('/data-table', function (){
    return view('page.data-table');
});

Route::get('/table', function (){
    return view('page.table');
});