@extends('layouts.master')

@section('judul')
Buat Account Baru
@endsection

@section('content')
<h2>Sign Up Form</h2>
<form action="/home" method="post">

    @csrf

    <label>Full Name :</label><br>
    <input type="text" name="fullname"><br>
    <label>Password :</label><br>
    <input type="password" name="pass"><br><br>

    <label>Gender :</label><br>
    <input type="radio" value="1" name="status">Male<br>
    <input type="radio" value="2" name="status">Female<br>
    <input type="radio" value="3" name="status">Other<br><br>

    <label>Nationality :</label><br><br>
    <select name="asal" id="">
        <option value="1">Indonesian</option>
        <option value="2">Inggris</option>
        <option value="3">Arabic</option>
        <option value="4">Japanese</option>
    </select><br><br>

    <label>Languange Spoken :</label><br><br>
    <input type="checkbox" value="1" name="bahasa">Bahasa Indonesia<br>
    <input type="checkbox" value="2" name="bahasa">English<br>
    <input type="checkbox" value="3" name="bahasa">Arabic<br>
    <input type="checkbox" value="4" name="bahasa">Japanese<br>
    <input type="checkbox" value="5" name="bahasa">Other<br><br>

    <label>Bio</label><br><br>
    <textarea name="Biodata" id="" cols="30" rows="5"></textarea><br><br>

    <input type="submit" value="Sign Up">
</form>
@endsection

    

