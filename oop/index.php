<?php
require_once ('ape.php');
require_once ('frog.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "coldblooded : " . $sheep->cold_blooded . "<br>"; // "no"

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>"; // "buduk"
echo "Legs : " . $kodok->legs . "<br>"; // 4
echo "coldblooded : " . $kodok->cold_blooded . "<br>"; // "no"
$kodok->jump(); // "Auooo"

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>"; // "kera sakti"
echo "Legs : " . $sungokong->legs . "<br>"; // 2
echo "coldblooded : " . $sungokong->cold_blooded . "<br>"; // "hop hop"
$sungokong->yell();

?>